Write-Host "Deploying updates to GitHub..."

if ([System.IO.File]::Exists(".\hugo.exe")) {
    .\hugo.exe
} else {
    hugo
}

cd public

git add .

git commit -m "rebuilding site $(Get-Date)"

git push

cd ..
