#!/bin/sh

echo "Deploying updates to GitHub..."

./bin/hugo

cd public

git add .

git commit -m "rebuilding site $(date)"

git push origin HEAD:master

cd ..
